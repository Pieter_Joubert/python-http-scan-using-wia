import requests
import json
import time
import threading
import hook

class myThread (threading.Thread):

   def __init__(self, file_name, parameters, i):
      threading.Thread.__init__(self)
      self.file_name = file_name
      self.parameters = parameters
      self.i = i

   def run(self):
      print ("Starting " + self.name)
      # Get lock to synchronize threads
      threadLock.acquire()
      self.result = send_data(self.file_name, self.parameters, self.i)
      # Free lock to release next thread
      threadLock.release()

threadLock = threading.Lock()
threads = []


def send_data(file_to_send, parameters, i):
   try:
      
      headers = {"authentication" : parameters['token']}
      print("URL: " + str(parameters['serverUrl']))
      url = parameters['serverUrl'] + "api/images"
      image = open(file_to_send, 'rb')
      files = {'images': image}

      try:
         r = requests.post(url, files=files, headers=headers)
 
      except Exception as e:
         files = None
         
         raise Exception(e)
      files = None
      
      json_text = json.loads(r.text)

      if ('hasError' in json_text):
         if (json_text['hasError'] == True):
            print(str(json_text['hasError']))
            raise Exception(json_text['message'])
         
      else:

         if (r.status_code == 200):
            print(parameters['scanOptions'])
            if 'multi' not in  parameters['scanOptions']:
               status = hook.hook_data(json_text['file']['_id'], parameters)
               code = status.status_code
            elif (parameters['scanOptions']['multi'] != True):
               status = hook.hook_data(json_text['file']['_id'], parameters)
               code = status.status_code
            else:
               #Hook happens when all images are uploaded
               code = 200
            if (r.status_code == 200 and code == 200):
               data_sending = "ok"
            elif (r.status_code != 200 and code == 200):
               data_sending = "Unable to send image. Status code " + str(r.status_code)
            elif (r.status_code == 200 and code != 200):
               data_sending = "Unable to hook image. Status code " + str(code)
            else:
               data_sending = "Something went wrong"
         else:
            data_sending = "Failed to upload image to server."

      return data_sending, json_text['file']['_id'], i

   except Exception as e:
      print("Error: " + str(e))
      files = None
      return e, None
   finally:
      if (image is not None):
         image.close()
