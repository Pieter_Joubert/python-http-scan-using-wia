import requests
import json
import datetime


def hook_data(image_id, parameters, page_num_and_id = None):

    try:
        date = parameters['date']
    except:
        d = datetime.date.today()
        today_timestamp = int(datetime.datetime(d.year,d.month,d.day,0,0).timestamp()) * 1000
        date = today_timestamp
    
    title_date = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    try:

        headers = {"content-type" : "application/json", "authentication" : parameters['token']}
        if (parameters['scanOptions']['multi'] != True):
            body = {
            "_id" : image_id,
            "title" : "scan " + title_date,
            "description" : "scanned date " + title_date,
            "summary" : "scan",
            "type" : "jpg",
            "partyId" : parameters['partyId'],
            "date" : date,
            "imageId" : image_id
            }

        elif (parameters['scanOptions']['multi'] == True):
            body = {
            "_id" : page_num_and_id[0]['imageId'],
            "title" : "scan " + title_date,
            "description" : "scanned date " + title_date,
            "summary" : "scan",
            "type" : "jpg",
            "partyId" : parameters['partyId'],
            "date" : date,
            "multi" : True,
            "imageId" : page_num_and_id[0]['imageId'],
            "subDocuments" : page_num_and_id
            }
            print("body : " + str(body))
        url = parameters['serverUrl'] + 'api/clinical/filesInfo'
        r = requests.post(url, headers=headers, data=json.dumps(body))
        
        return r


    except Exception as e:
        print(e)
        return e