import os
from win32com.client import Dispatch
import send
from PIL import Image

scanner_uuid = "{6BDD1FC6-810F-11D0-BEC7-08002BE2092F}"

def get_devices():
	# List all available devices 
	wia_dev_manager = Dispatch("WIA.DeviceManager")
	device_list = []
	
	for ix, device in enumerate(wia_dev_manager.DeviceInfos):
		data = { "_id" : str(device.DeviceID[-4:]), "name" : str(device.Properties("Name").Value)}
		print(str(device.DeviceID[:-5]))
		if (str(device.DeviceID[:-5]) == scanner_uuid):
			device_list.append(data)
	return device_list

def get_properties(device):
	wia = Dispatch("WIA.DeviceManager")
	try:
		for info in wia.DeviceInfos:
			if (str(info.DeviceID[:-5]) == scanner_uuid):
				if info.DeviceID[-4:]==device[-4:]:

					dev = info.Connect()

		functions = { "data" : {"_id" : device[-4:], "duplex" : False, "feeder" : False, "flatbed" : False}}
		i = 0
		try:
			dev.Properties("3088").Value = 5
			functions["data"]["duplex"] = True
		except:
			i += 1
			pass
		try:
			dev.Properties("3088").Value = 1
			functions["data"]["feeder"] = True
		except:
			i += 1
			pass

		try:
			dev.Properties("3088").Value = 0
			functions["data"]["flatbed"] = True
		except:
			if (i == 2):
				functions["data"]["flatbed"] = True
			
		return functions
	except:
		functions = { "hasError" : "true"}
		return functions


def scan(parameters, threads):

	dev = None
	message_header = "application/json"
	message_title = "nothing to scan"
	scan_futher = True
	return_image = parameters['scanOptions']['returnImage']
	image_name_front_converted = None
	image_name_back_converted = None
	try:
		i = 0
		wia = Dispatch("WIA.DeviceManager")

		for info in wia.DeviceInfos:
			
			if (str(info.DeviceID[:-5]) == scanner_uuid):
				if info.DeviceID[-4:]==parameters["scannerId"][-4:]:
					print(info.Properties("Name").Value)
					dev = info.Connect()

		
		if (dev == None):
			raise ValueError
		# print(dev.Properties("3088").Value)
		# dev.Properties("3086").Value = 37
		# dev.Properties("3087").Value = 0


		prop_val = 0

		try:
			no_scan = False
			if (dev.Properties("3088").Value == 5):
				if (dev.Properties("3087").Value == 0):
					scan_futher = False
					no_scan = True
		except:
			pass

		if (no_scan == True):
			raise Exception("No document to scan")

		if (parameters['scanOptions']['useDuplex'] == True and parameters['scanOptions']['useFeeder'] == False):
			dev.Properties("3088").Value = 5
			prop_val = 5
			print("Document handling status " + str(dev.Properties("3087").Value))
			if (dev.Properties("3087").Value == 0):
				scan_futher = False
		
		if (parameters['scanOptions']['useDuplex'] == True and parameters['scanOptions']['useFeeder'] == True):
			dev.Properties("3088").Value = 5
			prop_val = 5
			scan_futher = True
			print("Document handling status " + str(dev.Properties("3087").Value))
			if (dev.Properties("3087").Value == 0):
				scan_futher = False

		if (parameters['scanOptions']['useDuplex'] == False and parameters['scanOptions']['useFeeder'] == True):
			dev.Properties("3088").Value = 1
			prop_val = 1
			scan_futher = True
			print("Document handling status " + str(dev.Properties("3087").Value))
			if (dev.Properties("3087").Value == 0):
				scan_futher = False	

		
	
		# for ix,p in enumerate (dev.Properties):
		# 	print(f"{ix:3} {p.PropertyID:5} {p.Name:30} {p.Value}")
		# print(dev.Properties("3088").Value)

		# List Items of device
		for ix, item in enumerate(dev.Items):
			print(ix,item.ItemID)
			scanner = item

		# 0 0000\Root\Flatbed
		# 1 0000\Root\Feeder
		# scanner = dev.Items[1]
		# Select per number
		scanner.ItemID

		# '0000\\Root\\Flatbed'
		# scanner = scanner.Items(1)
		# Show all properties
		# for ix,p in enumerate (scanner.Properties):
		# 	print(f"{ix:3} {p.PropertyID:5} {p.Name:30} {p.Value}")

		# Set resolution
		try: 
			parameters['scanOptions']['quality']
		except:
			parameters['scanOptions']['quality'] = 'low'
			
		if (str(parameters['scanOptions']['quality']) == "high"):
			dpi = 400
		elif (str(parameters['scanOptions']['quality']) == "medium"):
			dpi = 300
		elif (str(parameters['scanOptions']['quality']) == "low"):
			dpi = 200

		scanner.Properties("Vertical Resolution").Value   = dpi
		scanner.Properties("Horizontal Resolution").Value = dpi

		for ix, item in enumerate(dev.Items):
			print(ix,item.ItemID)
			scanner = item
		# scanner.Properties("3096").Value = 0
	
		WIA_IMG_FORMAT_BMP = "{B96B3CAB-0728-11D3-9D7B-0000F81EF32E}"
		# for ix,p in enumerate (scanner.Properties):
		# 	print(f"{ix:3} {p.PropertyID:5} {p.Name:30} {p.Value}")
		while(scan_futher == True):
			if (prop_val == 5):
				image = scanner.Transfer(WIA_IMG_FORMAT_BMP)
				image2 = scanner.Transfer(WIA_IMG_FORMAT_BMP)
				
			else:

				image = scanner.Transfer()
				if ((parameters['scanOptions']['returnImage']) == True):

					image_name_front = "front_scan" + str(i)
					image_name_front_converted = "front_scan" + str(i) + 'converted'
					image.SaveFile("temp/" + image_name_front + ".bmp")
					img = Image.open("temp/" + image_name_front + ".bmp")
					img.save("temp/" + image_name_front_converted + ".jpg")
					os.remove("temp/" + image_name_front + ".bmp")

					return str(image_name_front_converted + '.jpg')
				if (prop_val == 1):
					scan_futher = True
				else:
					scan_futher = False
					
		
			if (prop_val == 5):

				image_name_front = "front_scan" + str(i)
				image_name_front_converted = "front_scan" + str(i) + 'converted'
				image.SaveFile("temp/" + image_name_front + ".bmp")
				img = Image.open("temp/" + image_name_front + ".bmp")
				img.save("temp/" + image_name_front_converted + ".jpg")
				os.remove("temp/" + image_name_front + ".bmp")

				thread = send.myThread("temp/" + image_name_front_converted + ".jpg", parameters, i,)
				thread.start()
				threads.append(thread)
				i += 1

				image_name_back = "back_scan" + str(i)
				image_name_back_converted = "back_scan" + str(i) + 'converted'
				image2.SaveFile("temp/" + image_name_back + ".bmp")
				img2 = Image.open("temp/" + image_name_back + ".bmp")
				img2.save("temp/" + image_name_back_converted + ".jpg")
				os.remove("temp/" + image_name_back + ".bmp")

				thread = send.myThread("temp/" + image_name_back_converted + ".jpg", parameters, i,)
				thread.start()
				threads.append(thread)
				i += 1
				
				image = None
				image2 = None

				if (parameters['scanOptions']['useFeeder'] == False):
					scan_futher = False
				if (dev.Properties("3087").Value == 0):
					scan_futher = False
			else:
			
				image_name_front = "front_scan" + str(i)
				image_name_front_converted = "front_scan" + str(i) + 'converted'
				image.SaveFile("temp/" + image_name_front + ".bmp")
				img = Image.open("temp/" + image_name_front + ".bmp")
				img.save("temp/" + image_name_front_converted + ".jpg")
				os.remove("temp/" + image_name_front + ".bmp")

				thread = send.myThread("temp/" + image_name_front_converted + ".jpg", parameters, i,)
				thread.start()
				threads.append(thread)
				i += 1
				image = None
				
			message_header = 'application/json'
			message_title = "Success"

	except ValueError:
		message_header = 'application/json'
		message_title = 'Unable to find item or not connected'

	except RuntimeError:
		message_header = 'application/json'
		message_title = 'Unable to receive image'
	
	except Exception as e:
		
		if (str(e) == "(-2147352567, 'Exception occurred.', (0, None, 'The user requested a scan and there are no documents left in the document feeder.', None, 0, -2145320957), None)"):
			message_header = 'application/json'
			message_title = "Success"	
			
		else:
			print(str(e))	
			message_header = 'application/json'
			message_title = str(e)	
			
	finally:
		
		if (return_image == True and image_name_front_converted != None):
			return ({"header" : "image/jpg", "message" : image_name_front_converted})
		
		
		return ({"header" : message_header, "message" : message_title,"amount" : i, "threads" : threads, "dev" : dev})