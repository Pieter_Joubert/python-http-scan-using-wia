from http.server import BaseHTTPRequestHandler,HTTPServer
import os
import json
from urllib.parse import urlparse
import wiascan
from win32com.client import Dispatch
import hook

PORT_NUMBER = 10060

def empty_temp():
	for file in os.listdir('temp'):
		os.remove("temp/" + file)
	os.rmdir("temp")


def send_response(self, mimetype, response):
			self.send_response(200)
			self.send_header('Content-type',mimetype)
			self.send_header("Access-Control-Allow-Origin","*")
			self.end_headers()
			self.wfile.write(response)

def check_params(parameters):

	try:
		error = False
		if (parameters["scanOptions"]["useDuplex"] == True and parameters["scanOptions"]["returnImage"] == True):
			error = True	
			
	except:
		pass
	if (error == True):
		raise Exception("Cannot return image with duplex scan")


	
	if ('scanOptions' not in parameters):
		parameters['scanOptions'] = {}

	if ('multi' not in parameters['scanOptions']):
		parameters['scanOptions']['multi'] = False
	
	if 'useDuplex' in parameters['scanOptions']:
		pass

	else:
		parameters['scanOptions']['useDuplex'] = False
		
		if 'useFeeder' in parameters['scanOptions']:
			pass
		
		else:
			parameters['scanOptions']['useFeeder'] = False
			if 'useFlatbed' in parameters['scanOptions']:
				pass

			else:
				parameters['scanOptions']['useFlatbed'] = True

				wia = Dispatch("WIA.DeviceManager")
				
				for info in wia.DeviceInfos:
					if info.DeviceID[-4:]==parameters["scannerId"][-4:]:
						dev = info.Connect()
				try:
					pass
				except:
					parameters['scanOptions']['useFlatbed'] = False
					dev.Properties("3088").Value = 1

	
	if 'serverUrl' not in parameters:
		raise Exception('No server Url specified')

	if 'partyId' not in parameters:
		raise Exception('No party ID specified')
							
	if 'returnImage' not in parameters['scanOptions']:
		if 'useFlatbed' in parameters['scanOptions']:
			if (parameters['scanOptions']['useFlatbed'] == True):
				parameters['scanOptions']['returnImage'] = True
			else:
				parameters['scanOptions']['returnImage'] = False
		else:
				parameters['scanOptions']['returnImage'] = False

#This class will handles any incoming request from
#the browser
class myHandler(BaseHTTPRequestHandler):
	def do_OPTIONS(self):
		self.send_response(200, "ok")
		self.send_header("Access-Control-Allow-Origin","*")
		self.send_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
		self.send_header("Access-Control-Allow-Headers", "Content-Type")
		
		self.end_headers()
		


	def do_POST(self):
		root_path = str(urlparse(self.path).path)
		image_id_array = []
		page_num_and_id = []
		try:
			if root_path=="/scan":

				temp_cleaned = None
				length = int(self.headers["Content-Length"])
				parameters = json.loads(self.rfile.read(length))
				parameters
				

				try:
					if (os.path.isdir("temp")):
						empty_temp()
					os.makedirs("temp")

				except Exception as e:
					os.makedirs("temp")
				
				threads = []

				check_params(parameters)

				print((parameters['scanOptions']['returnImage']))
		
				if ((parameters['scanOptions']['returnImage']) == True):
	
					response = None
					response = wiascan.scan(parameters, threads)
					print('image name ' + response['message'])
					if (str(response['header']) == 'image/jpg'):
						with open("temp/" + response['message'] + '.jpg', 'rb') as f:

							mimetype='image/jpg'
							response = f.read()
							send_response(self, mimetype, response)
					else:
							mimetype='application/json'
							response = bytes(json.dumps({"hasError" : True, "message" : str(response['message'])}), "utf8")
							send_response(self, mimetype, response)		
					return
				else:
					
					messages = wiascan.scan(parameters, threads)
		
				sent = "ok"
				for t in messages['threads']:
					t.join()

					if (t.result[0] != "ok"):
						for t in messages['threads']:
							t.join()
							sent = t.result[0]
							raise Exception(sent)
					image_id_array.append(t.result[1])
					page_num_and_id.append({ "imageId" : t.result[1], "sortOrder" : t.result[2]})

				print("Kom hier uit")
				print(page_num_and_id)
				print('geen fout')
				print(parameters['scanOptions']['multi'])
				print('geen fout')
				if (parameters['scanOptions']['multi'] == True):
					print('geen fout nogsteeds')
					# page_num_and_id.sort(key=sortOrder)
					sorted(page_num_and_id, key = lambda i: i['sortOrder']) 
					hook.hook_data(None, parameters, page_num_and_id)
					image_id_array = [page_num_and_id[0]['imageId']]
				empty_temp()
				
				temp_cleaned = True

				print(messages['message'])
				if (messages['message'] == "Success" and sent == "ok"):
					data = { "data" : {"imageIds" : image_id_array}}
					
				elif (messages['message'] != "Success" and sent == "ok"):
					data = {"hasError" : True, "message" : str(messages['message'])}

				elif (messages['message'] == "Success" and sent != "ok"):
					data = {"hasError" : True, "message" : str(sent)}

				else:
					data = {"hasError" : str(sent)}

				mimetype='application/json'
				response = bytes(json.dumps(data), "utf8")
				send_response(self, mimetype, response)
				# self.send_response(200)
				# self.send_header('Content-type',mimetype)
				# self.send_header("Access-Control-Allow-Origin","*")
				# self.end_headers()
				# self.wfile.write(bytes(json.dumps(data), "utf8"))

				return

		
		except IOError as e:
			empty_temp()
			print(str(e))
			if temp_cleaned is not None:
				empty_temp()
			mimetype='application/json'
			response = bytes(json.dumps({"hasError" : True, "message" : str(e)}), "utf8")
			send_response(self, mimetype, response)
			# self.send_response(200)
			# self.send_header('Content-type',mimetype)
			# self.send_header("Access-Control-Allow-Origin","*")
			# self.end_headers()
			# self.wfile.write(bytes(json.dumps({"hasError" : True, "message" : str(e)}), "utf8"))

		except Exception as e:
			print("Uiteindelik die uitsondering")
			print(str(e))
			if temp_cleaned is not None:
				empty_temp()
			mimetype='application/json'
			response = bytes(json.dumps({"hasError" : True, "message" : str(e)}), "utf8")
			send_response(self, mimetype, response)
			# self.send_response(200)
			# self.send_header('Content-type',mimetype)
			# self.send_header("Access-Control-Allow-Origin","*")
			# self.end_headers()
			# self.wfile.write(bytes(json.dumps({"hasError" : True, "message" : str(e)}), "utf8"))
	
	#Handler for the GET requests
	def do_GET(self):
		
		root_path = str(urlparse(self.path).path)
		try:
			if root_path=="/":
				message = "Application Running"
				# Send response status code
				mimetype = 'application/json'
				response =bytes(message, "utf8")
				send_response(self, mimetype, response)
				# self.send_response(200)
				# self.send_header('Content-type','application/json')
				# self.send_header("Access-Control-Allow-Origin","*")
				# self.end_headers()
				# self.wfile.write(bytes(message, "utf8"))
				return
		except Exception as e:
			print(str(e))

		try:	
			if root_path=="/devices":
				f = wiascan.get_devices()
	
				device_list = {"data" : f}
				print(json.dumps(device_list))
				self.send_response(200)	
				self.send_header('Content-type',"application/json")
				self.send_header("Access-Control-Allow-Origin","*")
				self.end_headers()
				self.wfile.write(bytes((json.dumps(device_list)), "utf8"))
				return
		except Exception as e:
			print(str(e))

		try:	
			if root_path=="/functions":
				query = (urlparse(self.path).query)
				f = wiascan.get_properties(query)
				self.send_response(200)	
				self.send_header('Content-type',"application/json")
				self.send_header("Access-Control-Allow-Origin","*")
				self.end_headers()
				self.wfile.write(bytes((json.dumps(f)), "utf8"))
				return
		except Exception as e:
			print(str(e))
		
try:
	#Create a web server and define the handler to manage the
	#incoming request
	server = HTTPServer(('', PORT_NUMBER), myHandler)
	print('Started httpserver on port ' , PORT_NUMBER)
	
	#Wait forever for incoming http requests
	server.serve_forever()

except KeyboardInterrupt:
	print ('^C received, shutting down the web server')
	server.socket.close()
